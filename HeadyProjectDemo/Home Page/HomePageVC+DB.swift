
//
//  HomePageVC+DB.swift
//  HeadyProjectDemo
//
//  Created by Urvish Modi on 12/02/18.
//  Copyright © 2018 Urvish Modi. All rights reserved.
//

import UIKit
import CoreData

extension HomePageVC
{
    func saveCategory(cat_id:Int,cat_name:String,tempContext:NSManagedObjectContext) {
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Tbl_category")
        request.returnsObjectsAsFaults = false
        request.predicate = NSPredicate(format: "id == %d", cat_id)
        do{
            let categoriesData = try tempContext.fetch(request)
            if categoriesData.count==0{
                
                let newCategory = NSEntityDescription.insertNewObject(forEntityName: "Tbl_category", into: tempContext)
                newCategory.setValue(cat_id, forKey: "id")
                newCategory.setValue(cat_name, forKey: "name")
                
                
            }
            else{
                if categoriesData.count == 1 {
                    let updateCategory=categoriesData[0] as! NSManagedObject
                    updateCategory.setValue(cat_name, forKey: "name")
                    
//                    let getAllProducts = self.getAllProducts(cat_id: cat_id, tempContext: tempContext)
//                    updateCategory.setValue(getAllProducts, forKey: "products")
                    
                }
                else{
                    for cat_obj in categoriesData{
                        tempContext.delete(cat_obj as! NSManagedObject)
                    }
                    let newCategory = NSEntityDescription.insertNewObject(forEntityName: "Tbl_category", into: tempContext)
                    newCategory.setValue(cat_id, forKey: "id")
                    newCategory.setValue(cat_name, forKey: "name")
                }
            }
        }
        catch let error{
            print("Error Des--\(error.localizedDescription)")
        }
        
    }
    
    func saveProduct_Normal(cat_id:Int,product_id:Int,product_name:String,product_date:Date,product_tax_name:String, product_tax_rate:Int, tempContext:NSManagedObjectContext) {
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Tbl_product")
        request.returnsObjectsAsFaults = false
        request.predicate = NSPredicate(format: "id == %d", product_id)
        do{
            let productsData = try tempContext.fetch(request)
            if productsData.count==0{
                
                let newProduct = NSEntityDescription.insertNewObject(forEntityName: "Tbl_product", into: tempContext)
                newProduct.setValue(product_id, forKey: "id")
                newProduct.setValue(product_name, forKey: "name")
                newProduct.setValue(product_date, forKey: "date_added")
                newProduct.setValue(product_tax_name, forKey: "tax_name")
                newProduct.setValue(product_tax_rate, forKey: "tax_rate")
                
                newProduct.setValue(cat_id, forKey: "cat_id")
                
                if let getCategory = self.getCategory(cat_id: cat_id, productItem: newProduct as! Tbl_product, tempContext: tempContext)
                {
                    newProduct.setValue(getCategory, forKey: "category")
                }
                
            }
            else{
                if productsData.count == 1 {
                    let updateProduct=productsData[0] as! NSManagedObject
                    
                    updateProduct.setValue(product_name, forKey: "name")
                    updateProduct.setValue(product_date, forKey: "date_added")
                    updateProduct.setValue(product_tax_name, forKey: "tax_name")
                    updateProduct.setValue(product_tax_rate, forKey: "tax_rate")
                    
                    updateProduct.setValue(cat_id, forKey: "cat_id")
                    
                    if let getCategory = self.getCategory(cat_id: cat_id, productItem: updateProduct as! Tbl_product, tempContext: tempContext)
                    {
                        updateProduct.setValue(getCategory, forKey: "category")
                    }
                    
                }
                else{
                    for product_obj in productsData{
                        tempContext.delete(product_obj as! NSManagedObject)
                    }
                    let newProduct = NSEntityDescription.insertNewObject(forEntityName: "Tbl_product", into: tempContext)
                    newProduct.setValue(product_id, forKey: "id")
                    newProduct.setValue(product_name, forKey: "name")
                    newProduct.setValue(product_date, forKey: "date_added")
                    newProduct.setValue(product_tax_name, forKey: "tax_name")
                    newProduct.setValue(product_tax_rate, forKey: "tax_rate")
                    
                    newProduct.setValue(cat_id, forKey: "cat_id")
                    
                    if let getCategory = self.getCategory(cat_id: cat_id, productItem: newProduct as! Tbl_product, tempContext: tempContext)
                    {
                        newProduct.setValue(getCategory, forKey: "category")
                    }
                    
                }
            }
        }
        catch let error{
            print("Error Des--\(error.localizedDescription)")
        }
    }
    
    func saveProduct_Type(product_id:Int,product_type_name:String,type_count:Int, tempContext:NSManagedObjectContext) {
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Tbl_product")
        request.returnsObjectsAsFaults = false
        request.predicate = NSPredicate(format: "id == %d", product_id)
        do{
            let productsData = try tempContext.fetch(request)
            if productsData.count==0{
                
                let newProduct = NSEntityDescription.insertNewObject(forEntityName: "Tbl_product", into: tempContext)
                newProduct.setValue(product_id, forKey: "id")
                newProduct.setValue(product_type_name, forKey: "type_name")
                newProduct.setValue(type_count, forKey: "type_count")
                
            }
            else{
                if productsData.count == 1 {
                    let updateProduct=productsData[0] as! NSManagedObject
                    updateProduct.setValue(product_type_name, forKey: "type_name")
                    updateProduct.setValue(type_count, forKey: "type_count")
                    
                }
                else{
                    for variant_obj in productsData{
                        tempContext.delete(variant_obj as! NSManagedObject)
                    }
                    let newProduct = NSEntityDescription.insertNewObject(forEntityName: "Tbl_product", into: tempContext)
                    newProduct.setValue(product_id, forKey: "id")
                    newProduct.setValue(product_type_name, forKey: "type_name")
                    newProduct.setValue(type_count, forKey: "type_count")
                    
                    
                }
            }
        }
        catch let error{
            print("Error Des--\(error.localizedDescription)")
        }
    }
    
    func saveVariant(product_id:Int,variant_id:Int,variant_price:Int,variant_size:Int,variant_color:String, tempContext:NSManagedObjectContext) {
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Tbl_all_variant")
        request.returnsObjectsAsFaults = false
        request.predicate = NSPredicate(format: "id == %d", variant_id)
        do{
            let varientsData = try tempContext.fetch(request)
            if varientsData.count==0{
                
                let newVariant = NSEntityDescription.insertNewObject(forEntityName: "Tbl_all_variant", into: tempContext)
                newVariant.setValue(variant_id, forKey: "id")
                newVariant.setValue(variant_price, forKey: "price")
                newVariant.setValue(variant_color, forKey: "color")
                newVariant.setValue(variant_size, forKey: "size")
                newVariant.setValue(product_id, forKey: "product_id")
                
                if let getProduct = self.getProduct(product_id: product_id, variantItem: newVariant as! Tbl_all_variant, tempContext: tempContext)
                {
                    newVariant.setValue(getProduct, forKey: "product")
                }
            }
            else{
                if varientsData.count == 1 {
                    let updateVariant=varientsData[0] as! NSManagedObject
                    
                    updateVariant.setValue(variant_price, forKey: "price")
                    updateVariant.setValue(variant_color, forKey: "color")
                    updateVariant.setValue(variant_size, forKey: "size")
                    updateVariant.setValue(product_id, forKey: "product_id")
                    
                    if let getProduct = self.getProduct(product_id: product_id, variantItem: updateVariant as! Tbl_all_variant, tempContext: tempContext)
                    {
                        updateVariant.setValue(getProduct, forKey: "product")
                    }
                }
                else{
                    for variant_obj in varientsData{
                        tempContext.delete(variant_obj as! NSManagedObject)
                    }
                    let newVariant = NSEntityDescription.insertNewObject(forEntityName: "Tbl_all_variant", into: tempContext)
                    newVariant.setValue(variant_id, forKey: "id")
                    newVariant.setValue(variant_price, forKey: "price")
                    newVariant.setValue(variant_color, forKey: "color")
                    newVariant.setValue(variant_size, forKey: "size")
                    newVariant.setValue(product_id, forKey: "product_id")
                    
                    if let getProduct = self.getProduct(product_id: product_id, variantItem: newVariant as! Tbl_all_variant, tempContext: tempContext)
                    {
                        newVariant.setValue(getProduct, forKey: "product")
                    }
                    
                }
            }
        }
        catch let error{
            print("Error Des--\(error.localizedDescription)")
        }
        
    }
    
    func getAllProducts() {
        
        let context : NSManagedObjectContext =  AppDelegate.sharedAppDelegate().managedObjectContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Tbl_product")
        
        let countSort = NSSortDescriptor(key: "type_count", ascending: false)
//        let priceSort = NSSortDescriptor(key: "ANY all_variant.price", ascending: true)
        
        request.sortDescriptors = [countSort]
        
        var contactData = [Tbl_product]()
        do{
            contactData=try context.fetch(request) as! [Tbl_product]
            
            self.mostViewedProduct = contactData.filter{$0.type_name == "most viewed products"}
            self.mostOrderedProduct = contactData.filter{$0.type_name == "most ordered products"}
            self.mostSharedProduct = contactData.filter{$0.type_name == "most shared products"}
            
        }
        catch let error{
            print("Error Des--\(error.localizedDescription)")
        }
    }
    
    func getCategory(cat_id:Int,productItem:Tbl_product,tempContext:NSManagedObjectContext) ->Tbl_category? {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Tbl_category")
        request.returnsObjectsAsFaults = false
        let pred1 = NSPredicate(format: "id == %d", cat_id)
        request.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [pred1])
        
        var category_obj : Tbl_category?
        do {
            let single_category = try tempContext.fetch(request)
            if single_category.count == 1
            {
                if let category = single_category[0] as? Tbl_category{
                    let products = category.products?.adding(productItem)
                    if let categoryProducts = products as NSSet?{
                        category.products = categoryProducts
                    }
                    
                    category_obj = category
                    
                }
            }
            
        }
        catch let error {
            print("Error Des--\(error.localizedDescription)")
        }
        
        return category_obj
    }
    
    func getProduct(product_id:Int,variantItem:Tbl_all_variant,tempContext:NSManagedObjectContext) -> Tbl_product? {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Tbl_product")
        request.returnsObjectsAsFaults = false
        let pred1 = NSPredicate(format: "id == %d", product_id)
        request.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [pred1])
        
        var product_obj : Tbl_product?
        
        do {
            let single_product = try tempContext.fetch(request)
            if single_product.count == 1
            {
                if let product = single_product[0] as? Tbl_product{
                    let variants = product.all_variant?.adding(variantItem)
                    
                    if let productVariants = variants as NSSet?{
                        product.all_variant = productVariants
                        
                    }
                    product_obj = product
                }
                
            }
            
        }
        catch let error {
            print("Error Des--\(error.localizedDescription)")
        }
        
        return product_obj
    }
    
}
