


//
//  HomePageVC.swift
//  HeadyProjectDemo
//
//  Created by Urvish Modi on 12/02/18.
//  Copyright © 2018 Urvish Modi. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import MBProgressHUD
import CoreData

class HomePageVC: UIViewController {
        
    @IBOutlet weak var tblData: UITableView!
    
    let apiName = "https://stark-spire-93433.herokuapp.com/json"
    
    var mostViewedProduct = [Tbl_product]()
    var mostOrderedProduct = [Tbl_product]()
    var mostSharedProduct = [Tbl_product]()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.getServerData(strApiName: apiName)
        self.navigationItem.title = "Heady Products"
        self.navigationController?.navigationBar.tintColor = self.view.tintColor

        self.setRightButton()
    }
    
    func setRightButton() {
        
        let categoryButton = UIBarButtonItem.init(title: "Filter", style: .plain, target: self, action: #selector(handleFilter))
        self.navigationItem.rightBarButtonItem = categoryButton
    }
    
    @objc func handleFilter() {
        
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "FilterVC") as! FilterVC
        self.navigationController?.pushViewController(controller, animated: true)
        
//        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
//
//        actionSheet.addAction(UIAlertAction(title: "Color", style: .default, handler: {
//            action in
//        }))
//
//        actionSheet.addAction(UIAlertAction(title: "Size", style: .default, handler: {
//            action in
//        }))
//
//        actionSheet.addAction(UIAlertAction(title: "Categories", style: .default, handler: {
//            action in
//        }))
//
//        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: {
//            action in
//        }))
//        self.present(actionSheet, animated: true, completion: nil)
        
    }
    
    func getServerData(strApiName:String) {
        
        self.showLoadingView()
        
        Alamofire.request(
            
            URL(string: strApiName)!,
            method: .get,
            parameters: nil)
            .validate()
            .responseJSON { (response) -> Void in
                
                self.hideLoadingView()
                
                let tempContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
                tempContext.parent = AppDelegate.sharedAppDelegate().managedObjectContext
                
                guard response.result.isSuccess else {
                    self.showAlert()
                    return
                }
                
                guard let apiResponse = response.result.value else{
                    self.showAlert()
                    return
                }
                
                let jsonData = JSON.init(apiResponse)
                for item in jsonData
                {
                    if item.0 == "categories"
                    {
                        for categoryItem in item.1
                        {
                            var category_id = 0
                            var category_name = ""
                            
                            if let tmp_category_id = categoryItem.1["id"].int
                            {
                                category_id = tmp_category_id
                            }
                            if let tmp_category_name = categoryItem.1["name"].string
                            {
                                category_name = tmp_category_name
                            }
                            self.saveCategory(cat_id: category_id, cat_name: category_name, tempContext: tempContext)
                            
                            for productItem in categoryItem.1["products"].arrayValue
                            {
                                var product_id = 0
                                var product_name = ""
                                var product_date = Date()
                                
                                if let tmp_product_id = productItem["id"].int
                                {
                                    product_id = tmp_product_id
                                }
                                if let tmp_product_name = productItem["name"].string
                                {
                                    product_name = tmp_product_name
                                }
                                if let tmp_product_date = productItem["date_added"].string
                                {
                                    product_date = self.stringToDate(strDate: tmp_product_date)
                                }
                                
                                var tax_name = ""
                                var tax_rate = 0
                                
                                if let taxDict = productItem["tax"].dictionary
                                {
                                    if let tmp_tax_name = taxDict["name"]?.string
                                    {
                                        tax_name = tmp_tax_name
                                    }
                                    
                                    if let tmp_tax_value = taxDict["value"]?.int
                                    {
                                        tax_rate = tmp_tax_value
                                    }
                                }
                                self.saveProduct_Normal(cat_id: category_id, product_id: product_id, product_name: product_name, product_date: product_date, product_tax_name: tax_name, product_tax_rate: tax_rate, tempContext: tempContext)

                                for variantItem in productItem["variants"].arrayValue
                                {
                                    var variant_id = 0,variant_size = 0,variant_price = 0
                                    
                                    var variant_color = ""
                                    
                                    if let tmp_variant_id = variantItem["id"].int
                                    {
                                        variant_id = tmp_variant_id
                                    }
                                    if let tmp_variant_color = variantItem["color"].string
                                    {
                                        variant_color = tmp_variant_color
                                    }
                                    if let tmp_variant_size = variantItem["size"].int
                                    {
                                        variant_size = tmp_variant_size
                                    }
                                    if let tmp_variant_price = variantItem["price"].int
                                    {
                                        variant_price = tmp_variant_price
                                    }
                                    
                                    self.saveVariant(product_id: product_id, variant_id: variant_id, variant_price: variant_price, variant_size: variant_size, variant_color: variant_color, tempContext: tempContext)
                                    
                                }
                            }
                            
//                            var childCategoryItemArr = [Int]()
//                            for chitCategoryItem in categoryItem.1["child_categories"].arrayValue
//                            {
//                                if let childCategoryItemId = chitCategoryItem.int
//                                {
//                                    childCategoryItemArr.append(childCategoryItemId)
//                                }
//                            }
//
//                            if childCategoryItemArr.count > 0
//                            {
//                                categoryObj.child_categories = childCategoryItemArr
//                                categoryObj.hasChildCategory = true
//                            }
//                            else
//                            {
//                                categoryObj.child_categories = [Int]()
//                                categoryObj.hasChildCategory = false
//                            }
                            
                        }
                        AppDelegate.sharedAppDelegate().saveTempContext(context: tempContext)
                    }
                    else if item.0 == "rankings"
                    {
                        for rankingItem in item.1
                        {
                            var type_name = ""
                            if let ranking_name = rankingItem.1["ranking"].string
                            {
                                type_name = ranking_name.lowercased()
                            }
                            for ranking_product_item in rankingItem.1["products"].arrayValue
                            {
                                var product_id = 0
                                var product_view_count = 0
                                
                                if let ranking_sub_id = ranking_product_item["id"].int
                                {
                                    product_id = ranking_sub_id
                                }
                                if let ranking_sub_view_count = ranking_product_item["view_count"].int
                                {
                                    product_view_count = ranking_sub_view_count
                                }
                                self.saveProduct_Type(product_id: product_id, product_type_name: type_name, type_count: product_view_count, tempContext: tempContext)
                            }
                        }
                        AppDelegate.sharedAppDelegate().saveTempContext(context: tempContext)
                    }
                }
                
                self.getAllProducts()
                
                DispatchQueue.main.async {
                    self.tblData.isHidden = false
                    self.tblData.reloadData()
                }
        }
        
    }
    
    func showAlert() {
        let alert = UIAlertController(title: "Alert", message: "Error getting data. Please check your network connection !", preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "Settings", style: .default, handler: { action in
            
            guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                return
            }
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                })
            }
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func showLoadingView() {
        let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
        loadingNotification.label.text = "Loading"
    }
    
    func hideLoadingView() {
        
        DispatchQueue.main.async {
            MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
        }
    }
    
    func stringToDate(strDate:String) ->Date {
        
        let df = DateFormatter.init()
        
        df.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        
        guard let date = df.date(from: strDate) else{
            
            return Date()
        }
        
        return date

    }

}
