


//
//  HomePageVC+TableView.swift
//  HeadyProjectDemo
//
//  Created by Urvish Modi on 12/02/18.
//  Copyright © 2018 Urvish Modi. All rights reserved.
//

import UIKit

extension HomePageVC:UITableViewDataSource,UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            if mostOrderedProduct.count > 5
            {
                return 5
            }
            else
            {
                return mostOrderedProduct.count
            }
        }
        else if section == 1
        {
            if mostSharedProduct.count > 5
            {
                return 5
            }
            else
            {
                return mostSharedProduct.count
            }
        }
        else
        {
            if mostViewedProduct.count > 5
            {
                return 5
            }
            else
            {
                return mostViewedProduct.count
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "cellHomeRankingSub") as! CellHomeRanking
        
        if indexPath.section == 0
        {
            cell.configureCellCommon(tblProductItem: self.mostOrderedProduct[indexPath.row], tmpPass: "purchased")
        }
        else if indexPath.section == 1
        {
            cell.configureCellCommon(tblProductItem: self.mostSharedProduct[indexPath.row], tmpPass: "shared")
        }
        else
        {
            cell.configureCellCommon(tblProductItem: self.mostViewedProduct[indexPath.row], tmpPass: "viewed")
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "VariantListVC") as! VariantListVC
        if indexPath.section == 0
        {
            if let tmp_variants = self.mostOrderedProduct[indexPath.row].all_variant?.allObjects, let variants = tmp_variants as? [Tbl_all_variant]
            {
                controller.variant_list = variants
                controller.product_name = "Most Ordered Products"
            }
        }
        else if indexPath.section == 1
        {
            if let tmp_variants = self.mostSharedProduct[indexPath.row].all_variant?.allObjects, let variants = tmp_variants as? [Tbl_all_variant]
            {
                controller.variant_list = variants
                controller.product_name = "Most Shared Products"
            }
        }
        else
        {
            if let tmp_variants = self.mostViewedProduct[indexPath.row].all_variant?.allObjects, let variants = tmp_variants as? [Tbl_all_variant]
            {
                controller.variant_list = variants
                controller.product_name = "Most Viewed Products"
            }
        }
        self.navigationController?.pushViewController(controller, animated: true)
    
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = UIView.init(frame: CGRect(x:0,y:0,width:self.view.frame.size.width,height:40))
        headerView.backgroundColor = UIColor.groupTableViewBackground
        
        let lblTitle = UILabel.init(frame: CGRect(x:15,y:12.5,width:self.view.frame.size.width-140,height:20))
        
        let btnSeeAll = UIButton.init(frame: CGRect(x:self.view.frame.size.width-120,y:12.5,width:110,height:20))
        btnSeeAll.setTitleColor(UIColor.darkGray, for: .normal)
        btnSeeAll.titleLabel?.font = UIFont.init(name: (btnSeeAll.titleLabel?.font.fontName)!, size: 13.0)
        
        if section == 0
        {
            lblTitle.text = "Most Ordered Products"
            
            if self.mostOrderedProduct.count > 5
            {
                btnSeeAll.setTitle("See all \(self.mostOrderedProduct.count) items", for: .normal)
                btnSeeAll.tag = 0
                btnSeeAll.addTarget(self, action: #selector(HomePageVC.handleSeeAll(sender:)), for: .touchUpInside)
                headerView.addSubview(btnSeeAll)
            }
        }
        else if section == 1
        {
            lblTitle.text = "Most Shared Products"
            if self.mostOrderedProduct.count > 5
            {
                btnSeeAll.setTitle("See all \(self.mostOrderedProduct.count) items", for: .normal)
                btnSeeAll.tag = 1
                btnSeeAll.addTarget(self, action: #selector(HomePageVC.handleSeeAll(sender:)), for: .touchUpInside)
                headerView.addSubview(btnSeeAll)
            }
            
        }else{
            
            lblTitle.text = "Most Viewed Products"
            if self.mostOrderedProduct.count > 5
            {
                btnSeeAll.setTitle("See all \(self.mostOrderedProduct.count) items", for: .normal)
                btnSeeAll.tag = 2
                btnSeeAll.addTarget(self, action: #selector(HomePageVC.handleSeeAll(sender:)), for: .touchUpInside)
                headerView.addSubview(btnSeeAll)
            }
        }
        
        headerView.addSubview(lblTitle)
        
        return headerView
    }
    
    @objc func handleSeeAll(sender:UIButton) {
        
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "ProductListVC") as! ProductListVC
        
        if sender.tag == 0
        {
            controller.product_list = self.mostOrderedProduct
            controller.nav_title = "Most Ordered Products"
        }
        else if sender.tag == 0
        {
            controller.product_list = self.mostSharedProduct
            controller.nav_title = "Most Shared Products"
        }
        else if sender.tag == 0
        {
            controller.product_list = self.mostViewedProduct
            controller.nav_title = "Most Viewed Products"
        }
        self.navigationController?.pushViewController(controller, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 45
    }
}
