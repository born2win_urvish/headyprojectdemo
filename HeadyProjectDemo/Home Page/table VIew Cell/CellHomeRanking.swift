//
//  CellHomeRanking.swift
//  HeadyProjectDemo
//
//  Created by Urvish Modi on 12/02/18.
//  Copyright © 2018 Urvish Modi. All rights reserved.
//

import UIKit

class CellHomeRanking: UITableViewCell {

    @IBOutlet weak var lblProductName: UILabel!
    
    @IBOutlet weak var lblVariants: UILabel!
    
    @IBOutlet weak var lblPriceStartsFrom: UILabel!
    
    @IBOutlet weak var lblViewsCount: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
//    func configureCellFromCategory(productItem:Tbl_product) {
//        
//        let textRange = NSMakeRange(0, "Checkout all \(productItem.variants.count) >>".count)
//        let attributedText = NSMutableAttributedString(string: "Checkout all \(productItem.variants.count) >>")
//        attributedText.addAttribute(NSAttributedStringKey.underlineStyle , value: NSUnderlineStyle.styleSingle.rawValue, range: textRange)
//        
//        self.lblProductName.text = productItem.name
//        self.lblVariants.attributedText = attributedText
//        self.lblPriceStartsFrom.text = "\(productItem.variants[0].price) Rs"
//        
//        self.lblViewsCount.text = ""
//    }
//    
    func configureCellCommon(tblProductItem:Tbl_product,tmpPass:String) {
        
        if let product_variats = tblProductItem.all_variant?.allObjects, product_variats.count > 0
        {
            let textRange = NSMakeRange(0, "Checkout all \(product_variats.count) >>".count)
            let attributedText = NSMutableAttributedString(string: "Checkout all \(product_variats.count) >>")
            attributedText.addAttribute(NSAttributedStringKey.underlineStyle , value: NSUnderlineStyle.styleSingle.rawValue, range: textRange)
            self.lblVariants.attributedText = attributedText
            
            if let tmp_variants = product_variats as? [Tbl_all_variant]
            {
                let sort_variants = tmp_variants.sorted { $0.price > $1.price }
                self.lblPriceStartsFrom.text = "Rs. \(sort_variants[0].price)"
            }
        }
        else
        {
            self.lblVariants.text = ""
            self.lblPriceStartsFrom.text = ""
        }

        
        if let product_name = tblProductItem.name
        {
            self.lblProductName.text = product_name
        }
        
        if tblProductItem.type_count > 0
        {
            self.lblViewsCount.text = "\(tblProductItem.type_count) \(tmpPass)"
        }
        else
        {
            self.lblViewsCount.text = "Exclusive New launch"
        }
        
    }
}
