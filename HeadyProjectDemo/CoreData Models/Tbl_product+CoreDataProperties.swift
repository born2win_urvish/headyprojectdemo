//
//  Tbl_product+CoreDataProperties.swift
//  HeadyProjectDemo
//
//  Created by Urvish Modi on 12/02/18.
//  Copyright © 2018 Urvish Modi. All rights reserved.
//
//

import Foundation
import CoreData


extension Tbl_product {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Tbl_product> {
        return NSFetchRequest<Tbl_product>(entityName: "Tbl_product")
    }

    @NSManaged public var cat_id: Int16
    @NSManaged public var date_added: NSDate?
    @NSManaged public var id: Int16
    @NSManaged public var name: String?
    @NSManaged public var tax_name: String?
    @NSManaged public var tax_rate: Int16
    @NSManaged public var type_count: Int64
    @NSManaged public var type_id: Int16
    @NSManaged public var type_name: String?
    @NSManaged public var all_variant: NSSet?
    @NSManaged public var category: Tbl_category?
    @NSManaged public var variants: NSSet?

}

// MARK: Generated accessors for all_variant
extension Tbl_product {

    @objc(addAll_variantObject:)
    @NSManaged public func addToAll_variant(_ value: Tbl_all_variant)

    @objc(removeAll_variantObject:)
    @NSManaged public func removeFromAll_variant(_ value: Tbl_all_variant)

    @objc(addAll_variant:)
    @NSManaged public func addToAll_variant(_ values: NSSet)

    @objc(removeAll_variant:)
    @NSManaged public func removeFromAll_variant(_ values: NSSet)

}

// MARK: Generated accessors for variants
extension Tbl_product {

    @objc(addVariantsObject:)
    @NSManaged public func addToVariants(_ value: Tbl_variants)

    @objc(removeVariantsObject:)
    @NSManaged public func removeFromVariants(_ value: Tbl_variants)

    @objc(addVariants:)
    @NSManaged public func addToVariants(_ values: NSSet)

    @objc(removeVariants:)
    @NSManaged public func removeFromVariants(_ values: NSSet)

}
