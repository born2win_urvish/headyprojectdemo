//
//  Tbl_category+CoreDataProperties.swift
//  HeadyProjectDemo
//
//  Created by Urvish Modi on 12/02/18.
//  Copyright © 2018 Urvish Modi. All rights reserved.
//
//

import Foundation
import CoreData


extension Tbl_category {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Tbl_category> {
        return NSFetchRequest<Tbl_category>(entityName: "Tbl_category")
    }

    @NSManaged public var id: Int16
    @NSManaged public var name: String?
    @NSManaged public var products: NSSet?

}

// MARK: Generated accessors for products
extension Tbl_category {

    @objc(addProductsObject:)
    @NSManaged public func addToProducts(_ value: Tbl_product)

    @objc(removeProductsObject:)
    @NSManaged public func removeFromProducts(_ value: Tbl_product)

    @objc(addProducts:)
    @NSManaged public func addToProducts(_ values: NSSet)

    @objc(removeProducts:)
    @NSManaged public func removeFromProducts(_ values: NSSet)

}
