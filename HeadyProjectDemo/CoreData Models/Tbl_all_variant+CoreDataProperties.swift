//
//  Tbl_all_variant+CoreDataProperties.swift
//  HeadyProjectDemo
//
//  Created by Urvish Modi on 12/02/18.
//  Copyright © 2018 Urvish Modi. All rights reserved.
//
//

import Foundation
import CoreData


extension Tbl_all_variant {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Tbl_all_variant> {
        return NSFetchRequest<Tbl_all_variant>(entityName: "Tbl_all_variant")
    }

    @NSManaged public var color: String?
    @NSManaged public var id: Int16
    @NSManaged public var price: Int64
    @NSManaged public var product_id: Int16
    @NSManaged public var size: Int16
    @NSManaged public var product: Tbl_product?

}
