//
//  Tbl_variants+CoreDataProperties.swift
//  HeadyProjectDemo
//
//  Created by Urvish Modi on 12/02/18.
//  Copyright © 2018 Urvish Modi. All rights reserved.
//
//

import Foundation
import CoreData


extension Tbl_variants {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Tbl_variants> {
        return NSFetchRequest<Tbl_variants>(entityName: "Tbl_variants")
    }

    @NSManaged public var color: Int16
    @NSManaged public var id: Int16
    @NSManaged public var price: Int16
    @NSManaged public var product_id: Int16
    @NSManaged public var size: Int16
    @NSManaged public var products: Tbl_product?

}
