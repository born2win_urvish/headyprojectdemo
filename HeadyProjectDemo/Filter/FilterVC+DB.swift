

//
//  FilterVC+DB.swift
//  HeadyProjectDemo
//
//  Created by Urvish Modi on 13/02/18.
//  Copyright © 2018 Urvish Modi. All rights reserved.
//

import UIKit
import CoreData

extension FilterVC
{
    func getAllSizes() -> [FilterType] {
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Tbl_all_variant")
        request.returnsObjectsAsFaults = false
        
        request.resultType = .dictionaryResultType
        request.returnsDistinctResults = true
        request.propertiesToFetch = ["size"]
        
        var sizeData = [FilterType]()
        
        do {
            let sizeArr = try AppDelegate.sharedAppDelegate().managedObjectContext.fetch(request)
            
            if let resultsDict = sizeArr as? [[String: Int]]
            {
                for item in resultsDict
                {
                    let colorObj = FilterType()
                    if let size_num = item["size"]
                    {
                        colorObj.filterName = "\(size_num)"
                        colorObj.itemsCount = self.getSizeCount(sizeNum: size_num).count
                        
                        sizeData.append(colorObj)
                    }
                }
            }
        } catch let error {
            print(error.localizedDescription)
        }
        return sizeData
    }
    
    func getSizeCount(sizeNum:Int) -> [Tbl_all_variant] {
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Tbl_all_variant")
        request.returnsObjectsAsFaults = false
        
        let sizePredicate = NSPredicate(format: "#size == %d", sizeNum)
        request.predicate = sizePredicate
        request.sortDescriptors = [NSSortDescriptor(key: "size", ascending: true)]
        
        var items = [Tbl_all_variant]()
        
        do {
            let sizeArr = try AppDelegate.sharedAppDelegate().managedObjectContext.fetch(request)
            
            if let tmp_items = sizeArr as? [Tbl_all_variant]
            {
                items = tmp_items
            }
            
        } catch let err {
            print(err.localizedDescription)
        }
        return items
    }
    
    
    func getAllColors() -> [FilterType] {
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Tbl_all_variant")
        request.returnsObjectsAsFaults = false
        
        request.resultType = .dictionaryResultType
        request.returnsDistinctResults = true
        request.propertiesToFetch = ["color"]
        
        var colorData = [FilterType]()
        
        do {
            let colorArr = try AppDelegate.sharedAppDelegate().managedObjectContext.fetch(request)
            if let resultsDict = colorArr as? [[String: String]]
            {
                for item in resultsDict
                {
                    let colorObj = FilterType()
                    if let color_name = item["color"]
                    {
                        colorObj.filterName = color_name
                        colorObj.itemsCount = self.getColorCount(colorName: color_name).count
                        
                        colorData.append(colorObj)
                    }
                }
            }
        } catch let error {
            print(error.localizedDescription)
        }
        return colorData
    }
    
    func getColorCount(colorName:String) -> [Tbl_all_variant] {
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Tbl_all_variant")
        request.returnsObjectsAsFaults = false
        request.predicate = NSPredicate(format: "color == %@", colorName)
        
        request.sortDescriptors = [NSSortDescriptor.init(key: "color", ascending: true, selector: #selector(NSString.localizedCompare(_:)))]
        
        var items = [Tbl_all_variant]()
        
        do {
            let colorArr = try AppDelegate.sharedAppDelegate().managedObjectContext.fetch(request)
            if let tmp_items = colorArr as? [Tbl_all_variant]
            {
                items = tmp_items
            }
        } catch let err {
            print(err.localizedDescription)
        }
        return items
    }
    
}
