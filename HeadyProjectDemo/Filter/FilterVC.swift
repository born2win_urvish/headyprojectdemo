

//
//  FilterVC.swift
//  HeadyProjectDemo
//
//  Created by Urvish Modi on 13/02/18.
//  Copyright © 2018 Urvish Modi. All rights reserved.
//

import UIKit

class FilterVC: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    var colorsData = [FilterType]()
    var sizeData = [FilterType]()

    override func viewDidLoad() {
        super.viewDidLoad()

        colorsData = self.getAllColors()
        sizeData = self.getAllSizes()
        
        self.navigationItem.title = "Filter by"
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0
        {
            return colorsData.count
        }
        else
        {
            return sizeData.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellFilter", for: indexPath) as! cellFilter
        
        if indexPath.section == 0
        {
            cell.lblFilterTitle.text = "\(colorsData[indexPath.row].filterName) "
            cell.lblItemsCount.text = "\(colorsData[indexPath.row].itemsCount) items"
        }
        else
        {
            cell.lblFilterTitle.text = "\(sizeData[indexPath.row].filterName) "
            cell.lblItemsCount.text = "\(sizeData[indexPath.row].itemsCount) items"
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.init(frame: CGRect(x:0,y:0,width:self.view.frame.size.width,height:40))
        headerView.backgroundColor = UIColor.groupTableViewBackground
        
        let label = UILabel.init(frame: CGRect(x:15,y:10,width:self.view.frame.size.width-20,height:20))
        
        label.font = UIFont.init(name: (label.font.fontName), size: 16.0)
        label.textColor = UIColor.darkGray
        
        if section == 0
        {
            label.text = "Color"
        }
        else
        {
            label.text = "Size"
        }
        headerView.addSubview(label)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "VariantListVC") as! VariantListVC
        
        if indexPath.section == 0
        {
            controller.variant_list = self.getColorCount(colorName: self.colorsData[indexPath.row].filterName)
            controller.product_name = "\(self.colorsData[indexPath.row].filterName) Color Products"
        }
        else
        {
            controller.variant_list = self.getSizeCount(sizeNum: Int(self.sizeData[indexPath.row].filterName)!)
            controller.product_name = "\(self.sizeData[indexPath.row].filterName) Size Products"
        }
        
        self.navigationController?.pushViewController(controller, animated: true)
        
    }

}
