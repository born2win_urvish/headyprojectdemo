


//
//  ProductListVC.swift
//  HeadyProjectDemo
//
//  Created by Urvish Modi on 13/02/18.
//  Copyright © 2018 Urvish Modi. All rights reserved.
//

import UIKit

class ProductListVC: UIViewController,UITableViewDataSource,UITableViewDelegate {

    var product_list = [Tbl_product]()
    var nav_title = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = nav_title
        
        // Do any additional setup after loading the view.
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return product_list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellHomeRankingSub") as! CellHomeRanking
        
        cell.configureCellCommon(tblProductItem: product_list[indexPath.row], tmpPass: "")
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "VariantListVC") as! VariantListVC
        if let tmp_variants = product_list[indexPath.row].all_variant?.allObjects, let variants = tmp_variants as? [Tbl_all_variant]
        {
            controller.variant_list = variants
        }
        controller.product_name = product_list[indexPath.row].name ?? ""
        self.navigationController?.pushViewController(controller, animated: true)
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
}
