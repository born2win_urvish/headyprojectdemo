


//
//  VariantListVC.swift
//  HeadyProjectDemo
//
//  Created by Urvish Modi on 13/02/18.
//  Copyright © 2018 Urvish Modi. All rights reserved.
//

import UIKit

class VariantListVC: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    var variant_list = [Tbl_all_variant]()
    var product_name = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = product_name

        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return variant_list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let obj = variant_list[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "VariantItemCell", for: indexPath) as! VariantItemCell
        
        var tax_rate = 0
        var tax_name = ""
        var tmp_count = 0
        var typeName = ""
        let price = Int(obj.price)
        
        if let productItem = obj.product
        {
            cell.lblProductName.text = productItem.name ?? ""
            
            tax_name = productItem.tax_name ?? ""
            tax_rate = Int(productItem.tax_rate)
            tmp_count = Int(productItem.type_count)
            
            if tmp_count > 0
            {
                if let tmp_type_name = productItem.type_name
                {
                    typeName = tmp_type_name.components(separatedBy: " ")[1]
                }
                
                cell.lblVariantMost.text = "\(typeName.capitalized): \(tmp_count)"
            }
            else
            {
                cell.lblVariantMost.text = ""
            }
        }
        
        let taxAmount = (price * tax_rate)/100
        
        cell.lblVariantColor.text = "Color: \(obj.color ?? "")"
        cell.lblVariantSize.text = "Size: \(obj.size)"
        cell.lblAmount.text = "\(price)"
        cell.lblTaxName.text = "\(tax_name) (\(tax_rate)%)"
        cell.lblTaxRate.text = "\(taxAmount)"
        cell.lblPayableAmount.text = "RS. \(price+taxAmount)"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 210
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }


}
