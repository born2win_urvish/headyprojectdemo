
//
//  VariantItemCell.swift
//  HeadyProjectDemo
//
//  Created by Urvish Modi on 13/02/18.
//  Copyright © 2018 Urvish Modi. All rights reserved.
//

import UIKit

class VariantItemCell: UITableViewCell {
    
    @IBOutlet weak var lblProductName: UILabel!
    
    @IBOutlet weak var lblVariantMost: UILabel!
    
    @IBOutlet weak var lblVariantColor: UILabel!
    
    @IBOutlet weak var lblVariantSize: UILabel!
    
    @IBOutlet weak var lblAmount: UILabel!
    
    @IBOutlet weak var lblTaxName: UILabel!
    
    
    @IBOutlet weak var lblTaxRate: UILabel!
    
    @IBOutlet weak var lblPayableAmount: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
